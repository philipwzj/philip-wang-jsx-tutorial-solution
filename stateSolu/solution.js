/* Solution for the react state exercise      
 * http://buildwithreact.com/tutorial/state
 */

var Board = React.createClass({
  render: function() {
    var className = "board";
    if (this.props.selected) {
      className += " selected";
    }
    return (
      <div className={className}>
        {this.props.index + 1}
      </div>
    );
  }
});

var BoardSwitcher = React.createClass({
  getInitialState: function() {
    return {
      current: 0
    };
  },
  
  toggleClicked: function() {
    this.setState({
      current: (this.state.current + 1) % 3
    });
  },

  render: function() {
    var boards = [];
    for (var ii = 0; ii < this.props.numBoards; ii++) {
      var isSelected = (ii === this.state.current);
      boards.push(
        <Board index={ii} selected={isSelected} key={ii} />
      );
    }
    
    return (
      <div>
        <div className="boards">{boards}</div>
        <button onClick={this.toggleClicked}>Toggle</button>
      </div>
    );
  }
});

ReactDOM.render(
  <BoardSwitcher numBoards={3} />,
  document.getElementById('container')
);