/* Solution for the react component exercise      
 * http://buildwithreact.com/tutorial/components
 * Purpose:
 * Create a VacancySign component that has a boolean prop hasvacancy. 
 * The component should render a div with either the text "Vacancy" or 
 * "No Vacancy" depending on the prop.
 */

var VacancySign = React.createClass({
	render: function() {
		return (
			<div>{this.props.hasvacancy?"Vacancy":"No Vacancy"}</div>
		);
	}
});

ReactDOM.render(
	<VacancySign hasvacancy={false} />,
	document.getElementById('container')
);
